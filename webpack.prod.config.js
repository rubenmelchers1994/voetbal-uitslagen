const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WebpackMd5Hash = require('webpack-md5-hash');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const TerserJSPlugin = require('terser-webpack-plugin');
const LiveReloadPlugin = require('webpack-livereload-plugin');
const nodeExternals = require('webpack-node-externals');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

// nodeEnv = (env.NODE_ENV && env.NODE_ENV == 'production') ? 'production' : 'development',
// console.log(env);
// const nodeEnv = env && env.NODE_ENV == 'production' ? 'production' : 'development';
// const isProd = nodeEnv === 'production';
// const env = process.env.NODE_ENV

// const config = {
//     mode: env || 'development'
// }

// console.log('KOEK', env);
module.exports = {
    optimization: {
        minimizer: [
            new UglifyJsPlugin({
                cache: true,
                parallel: true,
                sourceMap: true
            }),
            new TerserJSPlugin({}),
            new OptimizeCssAssetsPlugin({}),
        ],
    },
    entry: {
        // main: './src/js/index.js'
        index: path.join(__dirname, "src/js", "index.js"),
    },
    output: {
        filename: '[name].[chunkhash].js',
        path: path.join(__dirname, 'dist'),
        publicPath: '/',
    },
    devServer: {
        publicPath: './dist',
        watchContentBase: true
    },
    target: 'web',
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.scss$/,
                use: [
                    'style-loader',
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'postcss-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.css$/,
                use: [
                    MiniCssExtractPlugin.loader, 'css-loader'
                ]
            },
            {
                test: /\.jpg$/,
                use: [
                    {
                        loader: "url-loader"
                    }
                ]
            }
            // {
            //     test: /\.html$/,
            //     use: [
            //         {
            //             loader: "html-loader"
            //         }
            //     ]
            // }
        ]
    },
    plugins: [
        // new CleanWebpackPlugin(),
        new MiniCssExtractPlugin({
            filename: 'style.[contenthash].css',
            // filename: 'style.css',
        }),
        new HtmlWebpackPlugin({
            inject: true,
            hash: true,
            template: path.join(__dirname, "src/html", "index.html"),
            filename: 'index.html',
            chunks: ["index"],
            excludeChunks: ['server']
        }),
        new HtmlWebpackPlugin({
            inject: true,
            hash: true,
            template: path.join(__dirname, "src/html", "team.html"),
            filename: 'team.html',
            chunks: ["index"],
            excludeChunks: ['server']
        }),
        new HtmlWebpackPlugin({
            inject: true,
            hash: true,
            template: path.join(__dirname, "src/html", "personal.html"),
            filename: 'personal.html',
            chunks: ["index"],
            excludeChunks: ['server']
        }),
        new WebpackMd5Hash(),
        new LiveReloadPlugin(),
    ]

}