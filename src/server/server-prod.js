// const express = require('express');
const webpack = require('webpack');
const webpackMiddleware = require('webpack-dev-middleware');
const webpackConfig = require('../../webpack.prod.config.js');
// const path = require('path');
import path from 'path';
import express from 'express';

const app = express();
const DIST_DIR = __dirname;
const INDEX = path.join(DIST_DIR, "src/html", 'index.html');

// app.use(webpackMiddleware(webpack(webpackConfig)));
// app.get('/api', (req, res) => { });

// app.get('/team', (req, res) => { });

app.use(express.static(DIST_DIR));
app.get('*', (req, res) => {
    res.sendFile(INDEX);
})

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`App is listening to port ${PORT}`);
    console.log('oress Ctrl+C to quit.')
});