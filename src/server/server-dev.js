// const express = require('express');
// const webpack = require('webpack');
// const webpackMiddleware = require('webpack-dev-middleware');
// const webpackConfig = require('../../webpack.dev.config.js');
// const path = require('path');
import path from 'path';
import express from 'express';
import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import config from '../../webpack.dev.config.js';

const app = express();
const DIST_DIR = __dirname;
const INDEX = path.resolve(DIST_DIR, 'index.html');
const compiler = webpack(config);

console.log("TEST", INDEX);

app.use(webpackDevMiddleware(compiler, {
    publicPath: '/'
}));

app.use(webpackHotMiddleware(compiler));
// app.get('/api', (req, res) => { });

// app.get('/team', (req, res) => { });

// app.use(express.static(DIST_DIR));
// app.get('*', (req, res, next) => {
//     compiler.outputFileSystem.readFile(INDEX, (err, result) => {
//         if (err) {
//             console.log("ERRORR");
//             console.log("ERRORR");
//             console.log("ERRORR");
//             console.log("ERRORR");
//             return next(err)
//         }
//         res.set('content-type', 'text/html')
//         res.send(result)
//         res.end()
//     })
// })

app.get('/index', (req, res, next) => {
    compiler.outputFileSystem.readFile(INDEX, (err, result) => {
        if (err) {
            console.log('ERROR ON INDEX');
            console.log(err);
            return next(err)
        }
        res.set('content-type', 'text/html')
        res.send(result)
        res.end()
    })
})

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`App is listening to port ${PORT}`);
    console.log('oress Ctrl+C to quit.')
});