const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WebpackMd5Hash = require('webpack-md5-hash');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const TerserJSPlugin = require('terser-webpack-plugin');
const LiveReloadPlugin = require('webpack-livereload-plugin');
const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');

// nodeEnv = (env.NODE_ENV && env.NODE_ENV == 'production') ? 'production' : 'development',
// console.log(env);
// const nodeEnv = env && env.NODE_ENV == 'production' ? 'production' : 'development';

module.exports = {
    mode: 'development',
    optimization: {
        minimizer: [
            new TerserJSPlugin({}),
            new OptimizeCssAssetsPlugin({}),
        ],
    },
    entry: {
        // main: './src/js/index.js',
        // index: path.join(__dirname, "src/js", "index.js"),
        index: './src/js/index.js',
        main: [
            'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000', './src/js/index.js'
        ],
    },
    output: {
        filename: '[name].js',
        path: path.join(__dirname, 'dist'),
        publicPath: '/',
    },
    devServer: {
        publicPath: './dist',
        watchContentBase: true,
        proxy: {
            path: /./
        }
    },
    target: 'web',
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.scss$/,
                use: [
                    'style-loader',
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'postcss-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    MiniCssExtractPlugin.loader, 'css-loader'
                ]
            },
            {
                test: /\.(png|svg|jpg|gif)/,
                use: ['file-loader']
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: "html-loader"
                    }
                ]
            }
        ]
    },
    plugins: [
        // new CleanWebpackPlugin(),
        new MiniCssExtractPlugin({
            filename: 'style.[contenthash].css',
            // filename: 'style.css',
        }),
        new HtmlWebpackPlugin({
            inject: true,
            hash: true,
            // template: path.join(__dirname, "src/html", "index.html"),
            template: './src/html/index.html',
            filename: './index.html',
            chunks: ["index"],
            excludeChunks: ['server']
        }),
        // new HtmlWebpackPlugin({
        //     inject: true,
        //     hash: true,
        //     // template: path.join(__dirname, "src/html", "team.html"),
        //     template: './src/html/team.html',
        //     filename: './team.html',
        //     chunks: ["index"],
        //     excludeChunks: ['server']
        // }),
        // new HtmlWebpackPlugin({
        //     inject: true,
        //     hash: true,
        //     // template: path.join(__dirname, "src/html", "personal.html"),
        //     template: './src/html/personal.html',
        //     filename: './personal.html',
        //     // chunks: ["index"],
        //     excludeChunks: ['server']
        // }),
        new CopyWebpackPlugin([
            {
                from: 'src/php',
                to: './'
            }
        ]),
        new CopyWebpackPlugin([
            {
                from: 'src/img',
                to: 'img'
            }
        ]),
        // new WebpackMd5Hash(),
        // new LiveReloadPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin()
    ]

}